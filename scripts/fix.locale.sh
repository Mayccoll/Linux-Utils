#!/usr/bin/env bash

source <(curl -fsSL https://gitlab.com/mgldvd/Linux-Utils/-/raw/master/scripts/utils.sh)

rabbit

[ "$(id -u)" -eq 0 ] || SUDO=sudo

fancy_title 'Fix Locale'

set_locale() {
    export LANGUAGE=$1.UTF-8
    export LANG=$1.UTF-8
    export LC_ALL=$1.UTF-8
    $SUDO locale-gen $1.UTF-8
    $SUDO dpkg-reconfigure -f noninteractive locales
}

set_locale en_US

# set_locale es_ES

$SUDO apt-get install --reinstall -y debconf locales-all
