#!/usr/bin/env bash

source <(curl -fsSL https://gitlab.com/mgldvd/Linux-Utils/-/raw/master/scripts/utils.sh)

rabbit

[ "$(id -u)" -eq 0 ] || { command -v sudo >/dev/null 2>&1 && SUDO=sudo; }

fancy_title 'Run:' 'apt-get update'
$SUDO apt-get update -y > /dev/null

for pkg in curl git bash-completion zsh; do
  fancy_title "Install:" "$pkg"
  $SUDO apt-get install -y "$pkg" > /dev/null
done

fancy_title 'Install:' 'antigen'
curl -fsSL git.io/antigen -o ~/.antigen.zsh

ZSH_PATH=$(command -v zsh)

if ! grep -qxF "$ZSH_PATH" /etc/shells; then
    echo "$ZSH_PATH" | $SUDO tee -a /etc/shells > /dev/null
fi

$SUDO chsh -s "$ZSH_PATH" "$USER"

fancy_title "Add" ".zshrc"
curl -fsSL https://gitlab.com/mgldvd/Linux-Utils/-/raw/master/files/.zshrc -o "$HOME/.zshrc"

echo "exec zsh" >> ~/.bashrc

SHELL="$ZSH_PATH" exec "$ZSH_PATH"
