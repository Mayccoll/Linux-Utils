#!/usr/bin/env bash

# Colors
RS="\033[0m"

COLOR_RED="\033[1;31m"
COLOR_GREEN="\033[1;32m"
COLOR_COLOR_CYAN="\033[1;36m"

function_message_title () {
  echo -e "${COLOR_CYAN}"
  echo -e "# | ::::::::::::::::::::::::::::::::::::::::::::: | #"
  echo -e "# |      ${RS} $1 ${COLOR_CYAN}"
  echo -e "# | ::::::::::::::::::::::::::::::::::::::::::::: | #"
  echo -e "${RS}"
}

function_check_intall () {
  type -P $1 &>/dev/null && echo -e  "- Installed - ${COLOR_GREEN} Ok ${RS} - $1" || echo -e  "- Install - ${COLOR_RED} No ${RS} - $1"
}

SCRIPT_PATH=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

# install docker
function_message_title '- ** Install Docker **'
curl -sSL https://get.docker.com/ | sudo sh

# add my user to docker group
function_message_title '- ** add my user to docker group **'
sudo gpasswd -a $(whoami) docker

# restart docker
function_message_title '- ** Restart Docker **'
sudo service docker restart

function_check_intall docker
