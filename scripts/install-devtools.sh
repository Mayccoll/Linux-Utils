#!/usr/bin/env bash

source <(curl -fsSL https://gitlab.com/mgldvd/Linux-Utils/-/raw/master/scripts/utils.sh)

rabbit

[ "$(id -u)" -eq 0 ] || { command -v sudo >/dev/null 2>&1 && SUDO=sudo; }

fancy_title 'Run:' 'apt-get update'
$SUDO apt-get update -y

fancy_title 'Install:' 'Nix'
bash <(curl -fsSL https://nixos.org/nix/install) --daemon --yes

fancy_title 'Install:' 'Devbox'
FORCE=1 bash -c "$(curl -fsSL https://get.jetify.com/devbox)"

fancy_title 'Install:' 'Deno'
curl -fsSL https://deno.land/install.sh | bash -s -- -y

fancy_title 'Install:' 'Bun'
curl -fsSL https://bun.sh/install | bash

fancy_title 'Install:' 'Ollama'
curl -fsSL https://ollama.com/install.sh | bash

fancy_title 'Install:' 'Rust'
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | bash -s -- -y

fancy_title 'Install:' 'Go'
$SUDO apt-get install -y -qq golang-go > /dev/null

fancy_title 'Install:' 'Scrcpy'
$SUDO DEBIAN_FRONTEND=noninteractive \
    apt-get install -y -qq \
    ffmpeg libsdl2-2.0-0 adb wget gcc git pkg-config meson ninja-build \
    libsdl2-dev libavcodec-dev libavdevice-dev libavformat-dev \
    libavutil-dev libswresample-dev libusb-1.0-0 libusb-1.0-0-dev \
    android-tools-adb > /dev/null

TMP_DIR=$(mktemp -d)
cd "$TMP_DIR"
git clone --depth=1 https://github.com/Genymobile/scrcpy
cd scrcpy
./install_release.sh
rm -rf "$TMP_DIR"



