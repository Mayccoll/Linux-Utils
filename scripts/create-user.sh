#!/usr/bin/env bash

source <(curl -fsSL https://gitlab.com/mgldvd/Linux-Utils/-/raw/master/scripts/utils.sh)

rabbit

fancy_title 'Create a new user'

if [ -z "$1" ]; then
  ask_text "Enter the username to create" USERNAME
else
  USERNAME="$1"
fi

PASSWORD=$(openssl rand -base64 12)

ask_yes_no "Should the user be a sudo user?" SUDO_USER

sudo useradd -m -s /bin/bash "$USERNAME"
if [ $? -eq 0 ]; then
  echo "$USERNAME:$PASSWORD" | sudo chpasswd

  if [ "$SUDO_USER" = true ]; then
    sudo usermod -aG sudo "$USERNAME"
  fi

  sudo mkdir -p /home/$USERNAME/.ssh
  if [ -f /root/.ssh/authorized_keys ]; then
    sudo cp /root/.ssh/authorized_keys /home/$USERNAME/.ssh/
    sudo chmod 600 /home/$USERNAME/.ssh/authorized_keys
  else
    echo -e "\n${COLOR_WARN}Warning: /root/.ssh/authorized_keys not found. Skipping copy.${RS}"
  fi
  sudo chown -R $USERNAME:$USERNAME /home/$USERNAME
  sudo chmod 700 /home/$USERNAME/.ssh

  echo -e "\n${COLOR_GRAY}User '${COLOR_SUC}$USERNAME${COLOR_GRAY}' created successfully.${RS}\n"
  echo -e "${COLOR_GRAY}Password: ${COLOR_SUC}$PASSWORD${RS}\n"
else
  echo -e "${COLOR_DAN}Error: Could not create user '$USERNAME'. Check if the user already exists or if you have sufficient privileges.${RS}"
  exit 1
fi
