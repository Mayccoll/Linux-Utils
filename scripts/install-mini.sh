#!/usr/bin/env bash


source <(curl -fsSL https://gitlab.com/mgldvd/Linux-Utils/-/raw/master/scripts/utils.sh)

rabbit

fancy_title 'Install basic .bashrc'
curl -fsSL https://go.lanet.co/bashrc | bash

# fancy_title 'Install starship'
curl -fsSL https://go.lanet.co/starship | bash

# fancy_title 'Install zsh and tools'
curl -fsSL https://go.lanet.co/zsh | bash
