#!/usr/bin/env bash

RS="\e[0m"
CYAN="\e[0;36m"

function print_title () {
  local title="$1"
  echo -e "\n\n${CYAN}📍 ::::: $title ${RS}\n"
}

print_title 'Installing starship config'

STARSHIP_CONFIG="$HOME/.config/starship.toml"

cp $STARSHIP_CONFIG "$HOME/.config/starship.toml-$(date +%Y%m%d%H%M%S).bak"

curl -S https://gitlab.com/mgldvd/Linux-Utils/-/raw/master/files/starship/starship-gruvbox.toml > "$STARSHIP_CONFIG"
