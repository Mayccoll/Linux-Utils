#!/usr/bin/env bash

# Using Ubuntu
# https://github.com/nodesource/distributions

curl -fsSL https://deb.nodesource.com/setup_18.x | sudo -E bash -
sudo apt-get install -y nodejs

mkdir ~/.npm-global
npm config set prefix '~/.npm-global'
npm update -g
