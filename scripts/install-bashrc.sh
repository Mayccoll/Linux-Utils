#!/usr/bin/env bash

source <(curl -fsSL https://gitlab.com/mgldvd/Linux-Utils/-/raw/master/scripts/utils.sh)

rabbit

fancy_title 'Install Alias' "files: .custom_alias, .custom_path, .bashrc"

cp ~/.bashrc ~/.bashrc_$(date +%Y%m%d%H%M%S).bak

curl -S https://gitlab.com/mgldvd/Linux-Utils/-/raw/master/files/.custom_alias > "$HOME/.custom_alias"
curl -S https://gitlab.com/mgldvd/Linux-Utils/-/raw/master/files/.custom_path > "$HOME/.custom_path"
curl -S https://gitlab.com/mgldvd/Linux-Utils/-/raw/master/files/.bashrc > "$HOME/.bashrc"

source "$HOME/.bashrc"