#!/usr/bin/env bash

source <(curl -fsSL https://gitlab.com/mgldvd/Linux-Utils/-/raw/master/scripts/utils.sh)

rabbit


SCRIPT_PATH=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

[ "$(id -u)" -eq 0 ] || SUDO=sudo

fancy_title 'update'
$SUDO apt update

export DEBIAN_FRONTEND=noninteractive
$SUDO ln -fs /usr/share/zoneinfo/Etc/UTC /etc/localtime
$SUDO dpkg-reconfigure --frontend noninteractive tzdata

packages=(
  'dialog'
  'software-properties-common'
  'apt-utils'

  'git'
  'htop'
  'tree'
  'make'
  'neovim'

  'ack-grep'
  'arj'
  'autoconf'
  'build-essential'
  'cdbs'
  'check'
  'checkinstall'
  'devscripts'
  'dh-make'
  'git-extras'
  'mpack'
  'p7zip-rar'
  'rar'
  'sharutils'
  'unace'
  'uudeview'
  'zssh'

  'libbz2-dev'
  'libfreetype6-dev'
  'libgd-dev'
  'libgettextpo-dev'
  'libicu-dev'
  'libmcrypt-dev'
  'libmhash-dev'
  'libreadline-dev'
  'libxml-parser-perl'
  'libxml2-dev'
  'libxslt1-dev'
  're2c'
)

num_packages=${#packages[@]}

for ((i=0; i<num_packages; i++)); do
  pkg="${packages[$i]}"
  fancy_title "Package $((i+1))/$num_packages:" "$pkg"
  install_package "$pkg"
done
