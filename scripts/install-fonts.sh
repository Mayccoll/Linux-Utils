#!/usr/bin/env bash

declare -a fonts=(
    "Agave"
    "BitstreamVeraSansMono"
    "DejaVuSansMono"
    "DroidSansMono"
    "FiraCode"
    "GeistMono"
    "Hack"
    "SourceCodePro"
    "SpaceMono"
)
VERSION='3.3.0'

fonts_dir="${HOME}/.local/share/fonts"
mkdir -p "$fonts_dir"

for font in "${fonts[@]}"; do
    download_url="https://github.com/ryanoasis/nerd-fonts/releases/download/v${VERSION}/${font}.zip"
    echo -e "\e[33mDownloading $download_url\e[0m"
    curl -L -o "$fonts_dir/${font}.zip" "$download_url" && \
        unzip -o "$fonts_dir/${font}.zip" -d "$fonts_dir" -x "*.txt" -x "*.md" && \
        rm "$fonts_dir/${font}.zip"
done

find "$fonts_dir" -name '*Windows Compatible*' -delete

fc-cache -fv