# Linux & Utils (Linux Mint Cinnamon)

![Linux & Utils](./images/main-banner.jpg?inline=false)

Collection of steps and scripts that serves as an entry point for new linux mint installations.

**This repo is actually for personal use, but feel free to use or contribute, it if you find it useful.**

<br>

### 📜 **Before anything else install `curl`:**

```bash
[ "$(id -u)" -eq 0 ] || SUDO=sudo; $SUDO apt update && $SUDO apt install -y curl
```

### 📜 Install Mini - bash - zsh - starship 🟨 + 🟦 + 🟪

```bash
curl -fsSL https://go.lanet.co/mini | bash
```

### 📜 **Full init** 🟨 + 🟦 + 🟪 + 🟧 + 🟩 + 🟫 + 🟥

```bash
curl -fsSL https://go.lanet.co/init | bash
```

### 🟨 Install basic `.bashrc` with custom alias

```bash
curl -fsSL https://go.lanet.co/bashrc | bash
```

### 🟦 Install starship

```bash
curl -fsSL https://go.lanet.co/starship | bash
```

### 🟪 Install zsh and antigen

```bash
curl -fsSL https://go.lanet.co/zsh | bash
```

### 🟧 Install modern cli tools

```bash
curl -fsSL https://go.lanet.co/modern | bash
```

### 🟩 Install Basics

```bash
curl -fsSL https://go.lanet.co/basics | bash
```

### 🟫 Fix Locale

```bash
curl -fsSL https://go.lanet.co/fix-locale | bash
```

### 🟥 Set timezone New York

```bash
curl -fsSL https://go.lanet.co/tz-ny | bash
```

### 📜 Install Useful GUI Tools

```bash
curl -fsSL https://go.lanet.co/tools | bash
```

### 📜 Install Docker

```bash
curl -fsSL https://go.lanet.co/docker | bash
```

####  Install Docker - Linux mint

```bash
curl -fsSL https://gitlab.com/mgldvd/Linux-Utils/-/snippets/4797704/raw/main/docker-install.sh | bash
```

- Diff: https://www.diffchecker.com/BXgts4Kf/

- https://github.com/sindresorhus/guides/blob/main/docker-without-sudo.md

### 📜 Install Node.js (nvm)

```bash
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.40.0/install.sh | bash
```

Alternative:

```bash
bash -c "$(curl -fsSL https://go.lanet.co/nodejs)"
npm install -g n
```

more install options: https://github.com/nodesource/distributions

### 📜 Install Syncthing

```bash
bash -c "$(curl -fsSL https://go.lanet.co/syncthing)"
```

<br>

![Linux Commands](./images/title-banner.jpg)


### 📜 Startship config files:

**Mgldvd**

```bash
bash -c "$(curl -fsSL https://go.lanet.co/starship-config)"
```

**Mini**

```bash
bash -c "$(curl -fsSL https://go.lanet.co/starship-mini)"
```

**Starship Gruvbox**

```bash
bash -c "$(curl -fsSL https://go.lanet.co/starship-gruvbox)"
```

### 📜 Install Nerd Fonts

- https://github.com/ryanoasis/nerd-fonts/

- Agave
- BitstreamVeraSansMono
- DejaVuSansMono
- DroidSansMono
- FiraCode
- GeistMono
- Hack
- SourceCodePro
- SpaceMono

```bash
curl -fsSL https://go.lanet.co/install-fonts | bash
```

<br>
<br>

![Programs](./images/title-banner.jpg)

## 🖥️ **Programs**

- ▸ [Vagrant](https://releases.hashicorp.com/vagrant)
- ▸ [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
- ▸ [Obsidian](https://obsidian.md/)
- ▸ [sourcegit](https://github.com/sourcegit-scm/sourcegit)
- ▸ [neohtop](https://github.com/Abdenasser/neohtop/releases/tag/v1.0.8)
- [Angry IP Scanner](http://angryip.org/) `sudo apt install openjdk-21-jre `
- [Sublime text](http://www.sublimetext.com/3)
- [Krita](https://krita.org)
- [Ocenaudio](http://www.ocenaudio.com) - Audio Editor
- [Screamingfrog](https://www.screamingfrog.co.uk/seo-spider/)
- [Portmaster](https://safing.io/)
- [xpipe](https://xpipe.io/)
- [deck](https://get-deck.com/)

## 🛠️ **Utils**

📦 modern-unix

- https://github.com/ibraheemdev/modern-unix

```bash
sudo apt install -y stacer \
                    gparted \
                    lolcat \
                    inkscape \
                    calibre \
                    git-delta \
                    cpu-x \
                    safeeyes
```

📦 btop

```
sudo apt install -y bpytop
```

📦 gping

```
sudo apt install -y gping
```

📦 bat

```bash
sudo apt remove bat

cd /tmp

wget https://github.com/sharkdp/bat/releases/download/v0.24.0/bat-musl_0.24.0_amd64.deb

sudo apt install -y ./bat-musl_0.24.0_amd64.deb
```

📦  doggo

Command-line DNS client for humans

```bash
curl -sS https://raw.githubusercontent.com/mr-karan/doggo/main/install.sh | sh
```

<br>
<br>

## ❄️ Nix

- Install NIX

```bash
sh <(curl -L https://nixos.org/nix/install) --daemon --yes
```

- Install Devbox

```bash
curl -fsSL https://get.jetify.com/devbox | bash
```

```bash
nix profile install nixpkgs#sshs
nix profile install nixpkgs#act
```

<br>
<br>

## 🖥️ Apps

### 📦 Espanso

- https://espanso.org/docs/install/linux/#appimage-x11

### 📦 inotify

monitor events [Website](https://github.com/rvoicilas/inotify-tools)

```bash
sudo apt install -y inotify-tools
```

- USE: `while inotifywait -e close_write gulpfile.js; do gulp run; done`

### 📦 Ollama

```bash
curl -fsSL https://ollama.com/install.sh | sh
```

---

### 📦 Python3

```bash
sudo apt install -y python3-pip
sudo apt install -y pipx
```

#### yq

```bash
pipx install yq
```

- Command-line YAML/XML processor

#### Pypeek (Gif)

```bash
pipx install pypeek
```

- [Website 🌐](https://github.com/firatkiral/pypeek)

---

### 📦 Rust

```bash
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

#### Delta git

```bash
cargo install git-delta
```

- https://dandavison.github.io/delta/introduction.html

#### du-dust

```bash
cargo install du-dust
```

#### eza

```bash
cargo install eza
```

---

### 📦 Go

```bash
sudo apt install -y golang-go
```

#### scc

```bash
go install github.com/boyter/scc/v3@latest
```

- Count lines of code

---

### 📦 Android

```bash
sudo apt install -y ffmpeg libsdl2-2.0-0 adb wget \
    gcc git pkg-config meson ninja-build libsdl2-dev \
    libavcodec-dev libavdevice-dev libavformat-dev libavutil-dev \
    libswresample-dev libusb-1.0-0 libusb-1.0-0-dev android-tools-adb
cd /tmp && \
git clone https://github.com/Genymobile/scrcpy && \
cd scrcpy && \
./install_release.sh
```
> **run:** `scrcpy -s [SERIAL]`

> List devices: `adb devices`


### 📦 Install Ansible

```bash
sudo apt-add-repository -y ppa:ansible/ansible && \
sudo apt update && \
sudo apt install -y ansible
```

--------------------------------------------------------------------------------

<br>

![Npm](./images/title-banner.jpg)

## 📦 Npm

### Basics

```bash
npm update -g
```

```bash
npm i -g \
    live-server \
    npm-check-updates \
    @go-task/cli
```

```bash
npm i -g \
    pm2@latest\
    fast-cli
```

### More:

```bash
npm i -g @unibeautify/beautifier-js-beautify
npm i -g @unibeautify/cli
npm i -g @vue/cli
npm i -g align-yaml
npm i -g babel-cli
npm i -g babel-preset-es2015
npm i -g backup-docker
npm i -g cypress
npm i -g diff-so-fancy
npm i -g editorconfig
npm i -g electron
npm i -g eslint
npm i -g fkill-cli
npm i -g fx
npm i -g git-recall
npm i -g gulp
npm i -g js-beautify
npm i -g js-yaml
npm i -g jshint
npm i -g lighthouse
npm i -g livereload
npm i -g localtunnel
npm i -g nativefier
npm i -g node-sass
npm i -g node-xlsx2csv
npm i -g npmlist
npm i -g parcel-bundler
npm i -g replace-jquery
npm i -g sass-convert
npm i -g svgexport
npm i -g svgo
npm i -g uglify-js
npm i -g yarn
npm i -g zx
```

--------------------------------------------------------------------------------

<br>

![ More utils](./images/title-banner.jpg)


## More utils:

## 🔨

```bash
sudo apt install -y qshutdown
sudo apt install -y nmap
sudo apt install -y postgresql-client
sudo apt install -y luckybackup
sudo apt install -y wine
# Bless Hex Editor
sudo apt install -y bless
# Config Bandwidth
sudo apt install -y trickle
```

### 📦 Ghostwriter

```bash
sudo add-apt-repository -y ppa:wereturtle/ppa  && \
sudo apt update  && \
sudo apt install -y ghostwriter
```

### 📦 Qownnotes

```bash
sudo add-apt-repository -y ppa:pbek/qownnotes && \
sudo apt update && \
sudo apt install -y qownnotes
```

### 📦 Docker Color Output

```bash
sudo add-apt-repository -y ppa:dldash/core && \
sudo apt update && \
sudo apt install -y docker-color-output
```

### 📦 Webanalyze

```
go get -v -u github.com/rverton/webanalyze/cmd/webanalyze
webanalyze -update
```

### Mark my words

```bash
sudo add-apt-repository ppa:voldyman/markmywords
sudo apt update
sudo apt install -y mark-my-words
```

### Install Go For It!

```bash
sudo add-apt-repository -y ppa:mank319/go-for-it && \
sudo apt update && \
sudo apt install -y go-for-it
```


### Composer, Laravel, Artisan

```bash
sudo apt install -y php-cli && \
curl -sS https://getcomposer.org/installer | php && \
sudo mv composer.phar /usr/local/bin/composer

```

```bash
cat >> ${HOME}/.zshrc <<'endmsg'
# | ::::::: Composer ::::::::::::::::::::::::::::::::::::::::::::::::::::::: >>>
export PATH="$HOME/.config/composer/vendor/bin:$PATH"
# | ::::::: Composer ::::::::::::::::::::::::::::::::::::::::::::::::::::::: <<<
endmsg
```

### - Laravel Installer

```bash
composer global require "laravel/installer=~1.1"
```

- Create Artisan alias

```bash
cat >> ${HOME}/.zshrc <<'endmsg'
# | ::::::: Artisan php :::::::::::::::::::::::::::::::::::::::::::::::::::: >>>
alias artisan="php artisan"
# | ::::::: Artisan php :::::::::::::::::::::::::::::::::::::::::::::::::::: <<<
endmsg
```

### wkhtmltopdf

Convert html to pdf

```bash
$ sudo apt install -y  wkhtmltopdf

# Use Ex:
$ wkhtmltopdf http://google.com google.pdf
```


### pandoc

Universal document converter

``` bash
$ sudo apt install -y pandoc texlive-latex-recommended texlive-latex-extra texlive-fonts-recommended

## Use Ex:
$
pandoc -f markdown -t html README.md >> README.html
$
pandoc latex.md -o latex.pdf

```

### Java 8 or 9

```bash
$
# Java 8
sudo add-apt-repository -y ppa:webupd8team/java && \
sudo apt update && \
sudo apt install -y oracle-java8-installer

# Java 9
sudo add-apt-repository -y ppa:webupd8team/java && \
sudo apt update && \
sudo apt install -y oracle-java9-installer

# Make Java 9 Default
sudo apt install oracle-java9-set-default
```


--------------------------------------------------------------------------------

<br>

![Themes and Icons](./images/title-banner.jpg)

## Themes and Icons

## 🎨

### Vertex icons

https://www.deviantart.com/horst3180/art/Vertex-Icons-Beta-529266721

### Cairo-Dock

```bash
sudo add-apt-repository -y ppa:cairo-dock-team/ppa && \
sudo apt update && \
sudo apt install -y cairo-dock cairo-dock-plug-ins
```

### vertex

```bash
sudo add-apt-repository -y ppa:noobslab/icons && \
sudo apt update && \
sudo apt install -y vertex-icons
```

### Libra

```bash
sudo add-apt-repository -y ppa:noobslab/themes && \
sudo apt update && \
sudo apt install -y libra-theme
```

### Flattastic

```bash
sudo add-apt-repository -y ppa:noobslab/themes
sudo apt update
sudo apt install -y flattastic-suite
```

### iOS 7

```bash
sudo add-apt-repository -y ppa:noobslab/icons
sudo apt update
sudo apt install -y ieos7-icons
```

### Ambiance ¬ Radiante

```bash
sudo add-apt-repository -y ppa:ravefinity-project/ppa
sudo apt update
sudo apt install -y radiance-colors ambiance-colors
```

### Faience

```bash
# http://tiheum.deviantart.com/art/Faience-icon-theme-255099649
sudo add-apt-repository ppa:tiheum/equinox
sudo apt update
sudo apt install faience-theme faience-icon-theme
```

### Compass Icons

```bash
sudo ppa:noobslab/nitrux-os
sudo apt update
sudo apt install compass-icons
```

### Pacifica Icons

```bash
sudo add-apt-repository ppa:fsvh/pacifica-icon-theme
sudo apt update
sudo apt install pacifica-icon-theme
```

### Nitrux Icons

```bash
sudo add-apt-repository ppa:upubuntu-com/nitrux
sudo apt update
sudo apt install nitruxos
```

### Faience

``` bash
# http://tiheum.deviantart.com/art/Faience-icon-theme-255099649
sudo add-apt-repository ppa:tiheum/equinox
sudo apt update
sudo apt install faience-theme faience-icon-theme
```

--------------------------------------------------------------------------------

### Image Credits

- Photo by <a href="https://unsplash.com/@mitchel3uo?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Mitchell Luo</a> on <a href="https://unsplash.com/photos/4vBN9dMCip0?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>

- Photo by <a href="https://unsplash.com/@fakurian?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Milad Fakurian</a> on <a href="https://unsplash.com/photos/58Z17lnVS4U?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>
