#!/bin/bash

# ::::::: Reset color
RS="\e[0m"

# ::::::: Basic Colors
BLACK="\e[0;30m"
RED="\e[0;31m"
GREEN="\e[0;32m"
YELLOW="\e[0;33m"
BLUE="\e[0;34m"
PURPLE="\e[0;35m"
CYAN="\e[0;36m"
WHITE="\e[0;37m"


SCRIPT_PATH=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

function_message_title () {
  echo -e "${CYAN}"
  echo -e "# | ::::::::::::::::::::::::::::::::::::::::::::: | #"
  echo -e "# |      ${RS} $1 ${CYAN}"
  echo -e "# | ::::::::::::::::::::::::::::::::::::::::::::: | #"
  echo -e "${RS}"
}


function_delete_beetwen () {
  eval LINE_BEG="$1"
  eval LINE_END="$2"
  eval FILE_ZSH="$3"
  sed -e "/${LINE_BEG}/,/${LINE_END}/d"  "${FILE_ZSH}" > "${FILE_ZSH}_tmp"
  cp "${FILE_ZSH}_tmp" "${FILE_ZSH}"
  rm "${FILE_ZSH}_tmp"
}



# - **Install Stars**
# ==============================================

function_message_title '- Install Stars -'


FILE_ZSH="${HOME}/.zshrc"
LINE_BEG='
# | ::::::: Stars :::::::::::::::::::::::::::::::::::::::::::::::::::::::::: >>>
'
LINE_BEG="${LINE_BEG//[$'\n']}"

LINE_END='
# | ::::::: Stars :::::::::::::::::::::::::::::::::::::::::::::::::::::::::: <<<
'
LINE_END="${LINE_END//[$'\n']}"


echo -e "
Option 1:
\e[38;5;242m
☆ ＊.  ･ ·̩　｡☆ ｡ ★ ﾟ✦　｡☆ﾟ｡ ＊　 ｡*　+ ✧ ｡☆　＊　･ ｡☆+　＊　･ ｡★ 　* ･｡*　✨
${RS}
Option 2:
                                                                        　✨
${RS}
Option 3:
\e[38;5;242m
　･ ·̩　 ｡　☆　　ﾟ｡ ＊ 　 ｡*　　+　 　＊ 　･ ｡☆+　　＊ 　･ ｡☆ 　* ･｡*　+　 ＊ 　
＊ 　 ｡*　　+　✦　＊　･ ｡☆ 　ﾟ･　｡ﾟ･　☆ﾟ　+ ｡　☆　　ﾟ｡･ ·̩　　｡ﾟ･　･　☆ﾟ+ ｡　☆　
 ☆　★　*　　* 　 。 　 ･ ·̩　 ｡　☆　✦　　ﾟ｡　☆　　*　* 　　☆ﾟ　+
ﾟ･　｡ﾟ･　☆ﾟ　+ 　☆　✦　*　　　* 　 。＊ 　 ｡*　　+　 　＊ 　･ ｡☆　☆ﾟ　+ 　☆　
${RS}
"

function_delete_beetwen "\${LINE_BEG}" "\${LINE_END}" "\${FILE_ZSH}"

read -r -p "Select one option: " response

case "$response" in

  1)
echo "${LINE_BEG}" >> ${FILE_ZSH}
cat >> ${FILE_ZSH} <<'endmsg'
echo -e '\e[38;5;242m
☆ ＊.  ･ ·̩　｡☆ ｡ ★ ﾟ✦　｡☆ﾟ｡ ＊　 ｡*　+ ✧ ｡☆　＊　･ ｡☆+　＊　･ ｡★ 　* ･｡*　✨'
endmsg
echo "${LINE_END}" >> ${FILE_ZSH}
    ;;

  2)
echo "${LINE_BEG}" >> ${FILE_ZSH}
cat >> ${FILE_ZSH} <<'endmsg'
echo -e '
                                                                        　✨
'
endmsg
echo "${LINE_END}" >> ${FILE_ZSH}
    ;;

  3)
echo "${LINE_BEG}" >> ${FILE_ZSH}
cat >> ${FILE_ZSH} <<'endmsg'
if [ -x "$(command -v lolcat)" ]; then
echo '
　･ ·̩　 ｡　☆　　ﾟ｡ ＊ 　 ｡*　　+　 　＊ 　･ ｡☆+　　＊ 　･ ｡☆ 　* ･｡*　+　 ＊ 　
＊ 　 ｡*　　+　✦　＊　･ ｡☆ 　ﾟ･　｡ﾟ･　☆ﾟ　+ ｡　☆　　ﾟ｡･ ·̩　　｡ﾟ･　･　☆ﾟ+ ｡　☆　
 ☆　★　*　　* 　 。 　 ･ ·̩　 ｡　☆　✦　　ﾟ｡　☆　　*　* 　　☆ﾟ　+
ﾟ･　｡ﾟ･　☆ﾟ　+ 　☆　✦　*　　　* 　 。＊ 　 ｡*　　+　 　＊ 　･ ｡☆　☆ﾟ　+ 　☆

· · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · ·
' | lolcat
else
echo -e "\e[3$(( $RANDOM * 6 / 32767 + 1 ))m
　･ ·̩　 ｡　☆　　ﾟ｡ ＊ 　 ｡*　　+　 　＊ 　･ ｡☆+　　＊ 　･ ｡☆ 　* ･｡*　+　 ＊ 　
＊ 　 ｡*　　+　✦　＊　･ ｡☆ 　ﾟ･　｡ﾟ･　☆ﾟ　+ ｡　☆　　ﾟ｡･ ·̩　　｡ﾟ･　･　☆ﾟ+ ｡　☆　
 ☆　★　*　　* 　 。 　 ･ ·̩　 ｡　☆　✦　　ﾟ｡　☆　　*　* 　　☆ﾟ　+
ﾟ･　｡ﾟ･　☆ﾟ　+ 　☆　✦　*　　　* 　 。＊ 　 ｡*　　+　 　＊ 　･ ｡☆　☆ﾟ　+ 　☆
"
fi
endmsg
echo "${LINE_END}" >> ${FILE_ZSH}
    ;;

    *)
    echo "exit"
    ;;
esac
