# -*- mode: ruby -*-
# vi: set ft=ruby :
Vagrant.configure("2") do |config|
  # config.vm.box = "gusztavvargadr/ubuntu-desktop"
  # config.vm.box_version = "2404.0.2409"
  config.vm.box = "aaronvonawesome/linux-mint-22-cinnamon"
  config.vm.box_version = "2.1.0"
  config.vm.synced_folder "./", "/home/vagrant/share"

  config.vm.provider "virtualbox" do |vb|
    vb.gui = true
    vb.memory = "4096"
    vb.cpus = 2
    vb.customize ["modifyvm", :id, "--clipboard", "bidirectional"]
    vb.customize ["modifyvm", :id, "--vram", "128"]
  end

  # Provision to enable autologin, install lightdm faster, and auto-open terminal
  config.vm.provision "shell", inline: <<-SHELL
    # Speed up package installation
    export DEBIAN_FRONTEND=noninteractive

    # Update and install LightDM (only if not already installed)
    if ! dpkg -l | grep -q lightdm; then
      sudo apt-get update -q > /dev/null
      sudo apt-get install -yq lightdm
    fi

    # Set up LightDM auto-login
    sudo bash -c 'cat > /etc/lightdm/lightdm.conf <<EOF
[Seat:*]
autologin-user=vagrant
autologin-user-timeout=0
EOF'
    sudo systemctl restart lightdm

    # Create autostart directory if not exists
    mkdir -p /home/vagrant/.config/autostart

    # Create autostart entry to open a terminal
    cat <<EOF > /home/vagrant/.config/autostart/open-terminal.desktop
[Desktop Entry]
Type=Application
Exec=gnome-terminal
Hidden=false
NoDisplay=false
X-GNOME-Autostart-enabled=true
Name=Open Terminal
EOF

    # Ensure correct ownership
    chown -R vagrant:vagrant /home/vagrant/.config/autostart
  SHELL
end
